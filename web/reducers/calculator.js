import Store from '../store/calculator';
export const initialState = Store;

export default function userReducer(state = initialState, action) {
  const operation = state.operations + action.value;
  const resultString = operation.replace(',', '')
  switch (action.type) {
    case 'ADD_OPERANDS':
      return Object.assign({}, state, {
        result: eval(resultString),
        operations: operation
      });
    case 'ADD_OPERATOR':
      return Object.assign({}, state, {
        operations: state.operations + action.value
      });

    case 'ON_CLEAR':
      return Object.assign({}, state, {
        operations: '',
        result: ''
      });

    case 'EVALUATE':
      return Object.assign({}, state, {
        operations: eval(resultString).toString(),
        result: eval(resultString)
      });

    default:
      return state;
  }
}