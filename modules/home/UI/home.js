import React, { Component } from 'react';
import { Row, Col, Container } from 'reactstrap';
import Calculator from './Calculator/index';
import '../styles/home.scss';

class Index extends Component {

  render() {
    return (
      <Container className="home-page">
        <Row>
          <Col sm={{size: 4, offset: 4}} >
            <div className="calculator-container">
              <Calculator />
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Index;