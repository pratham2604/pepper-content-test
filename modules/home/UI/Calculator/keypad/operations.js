import React, { Component } from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';

const Operations = [{
  icon: '/',
  name: 'division',
  operation : '/'
}, {
  icon: '*',
  name: 'multiply',
  operation : '*'
}, {
  icon: '-',
  name: 'subtract',
  operation : '-'
}, {
  icon: '+',
  name: 'add',
  operation : '+'
}]

class Index extends Component {
  render() {
    const { dispatch } = this.props;
    return (
      <div className="operations-container">
        {Operations.map((operation, index) => {
          const { icon, name } = operation;
          return (
            <div key={name} className="operation" onClick={() => {dispatch({type: 'ADD_OPERATOR', value: operation.operation})}}>
              <div>{icon}</div>
            </div>
          )
        })}
        <div className="operation right-bottom-curve" onClick={() => {dispatch({type: 'EVALUATE', value: ''})}}>
          <div>=</div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);