import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import Operations from './operations';

const Index = (props) => (
  <div className="keypad-container">
    <Operands dispatch={props.dispatch} />
    <Operations />
  </div>
)

const Operands = (props) => (
  <div className="operands-container">
    <Actions dispatch={props.dispatch}/>
    <Numbers dispatch={props.dispatch}/>
  </div>
)

const NumKey = (props) => (
  <div className="cell flex-1" onClick={() => {props.dispatch({type: 'ADD_OPERANDS', value: props.value})}}>{props.value}</div>
)

class Numbers extends Component {
  render() {
    const { dispatch } = this.props;
    return (
      <div className="numbers-container">
        <div className="number-row">
          <NumKey value={7} dispatch={dispatch} />
          <NumKey value={8} dispatch={dispatch} />
          <NumKey value={9} dispatch={dispatch} />
        </div>
        <div className="number-row">
          <NumKey value={4} dispatch={dispatch} />
          <NumKey value={5} dispatch={dispatch} />
          <NumKey value={6} dispatch={dispatch} />
        </div>
        <div className="number-row">
          <NumKey value={1} dispatch={dispatch} />
          <NumKey value={2} dispatch={dispatch} />
          <NumKey value={3} dispatch={dispatch} />
        </div>
        <div className="number-row">
          <div className="cell left-bottom-curve" style={{width: '66.667%'}} onClick={() => {dispatch({type: 'ADD_OPERANDS', value: 0})}}>0</div>
          <div className="cell" style={{width: '33.334%'}} onClick={() => {dispatch({type: 'ADD_OPERANDS', value: ','})}}>,</div>
        </div>
      </div>
    )
  }
}

const Actions = (props) => (
  <div className="actions-container">
    <div className="cell flex-1 actions" onClick={() => {props.dispatch({type: 'ON_CLEAR'})}}>C</div>
    <div className="cell flex-1 actions">+/-</div>
    <div className="cell flex-1 actions" onClick={() => {props.dispatch({type: 'ADD_OPERATOR', value: '%'})}}>%</div>
  </div>
)

const mapStateToProps = (state, ownProps) => ({
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);