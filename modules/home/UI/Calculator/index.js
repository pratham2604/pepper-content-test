import React from 'react';
import Keypad from './keypad/index';
import DisplaySection from './display';

export default () => (
  <div className="calculator-model">
    <DisplaySection />
    <Keypad />
  </div>
)