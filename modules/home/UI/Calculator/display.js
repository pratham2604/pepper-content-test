import { result } from 'lodash';
import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';

class Index extends Component {
  render() {
    const { operations, result } = this.props; 
    return (
      <div className="display-container">
        <div className="operation">
          {operations}
        </div>
        <div className="result">
          {result}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  operations: state.calculator.operations,
  result: state.calculator.result,
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);